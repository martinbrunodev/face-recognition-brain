# Face Recognition Brain 

## 1. Clone this Repo
https://github.com/martin-developer/face-recognition-brain.git

## 2. Install dependencies
npm install

## 3. Run React dev server (http://localhost:3000)
npm start
